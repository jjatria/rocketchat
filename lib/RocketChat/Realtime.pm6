unit class RocketChat::Realtime;

use Cro::WebSocket::Client;

has Bool     $!debug  is built = %*ENV<ROCKET_CHAT_DEBUG>.so;
has Str      $.server is required;
has Int      $.version   = 1;
has Int      $.timeout   = 30;
has Supplier $!supplier .= new;
has Promise  %!messages;

has Cro::WebSocket::Client::Connection $!conn;
has Cro::WebSocket::Client             $!client .= new(
    content-type => 'application/json',
    http         => ['1.1'],
    uri          => $!server,
    json         => True,
);

#|(
    Returns a Promise that will be kept when a connection is established.
)
method connect ( --> Promise ) {
    die 'Already connected!' if self.connected;

    $!client.connect.then: {
        $!conn = .result;

        start react whenever $!conn.messages -> $msg {
            my $payload = await $msg.body;
            my $event = $payload<msg> // 'unknown';

            $!supplier.emit: $payload;

            self!log: '<<', $payload;

            if $event eq 'ping' {
                self!send: { msg => 'pong' };
                next;
            }

            .keep: $payload with %!messages{ $payload<id> // '' }:delete;

            LEAVE given $event {
                when 'error' {
                    my $id = $payload<offendingMessage><id> // '';
                    .keep: $payload with %!messages{$id}:delete;
                }

                when 'ready' {
                    for $payload<subs>.List {
                        .keep: $payload with %!messages{$_}:delete;
                    }
                }
            }
        }

        self.send: {
            msg     => 'connect',
            version => ~$!version,
            support => ['1'],
        }
    }
}

#|(
    Returns True if a connection has been established and has not yet
    been closed.
)
method connected ( --> Bool ) {
    return False if not $!conn;
    return False if     $!conn.closed;
    return True;
}

#|(
    Closes the connection to the web socket and returns a Promise that will be
    kept when the connection is effectively closed. Takes an optional code to
    send when closing the connection (defaults to 1000) and a C<timeout>
    parameter to specify how long to wait for the server to close the
    connection. Setting this to 0 forces an immediate termination.

    Calling this method will also close all the supplies created using
    C<.subscribe>.
)
method close ( $code = 1000, :$timeout --> Promise ) {
    return Promise.kept unless $!conn;
    $!conn.close( $code, :$timeout );
}

method reconnect ( |c --> Promise ) {
    start {
        await self.close(|c);
        await self.connect;
    }
}

#|(
    Sends an asynchronous request to the server and returns a Promise
    that, when fulfilled, will return whatever the server responded.
)
method send ( %msg --> Promise ) {
    use UUID;

    my $id = %msg<id> = ~ UUID.new: version => 4;
    %!messages{$id} = Promise.new;
    self!send: %msg;
    return %!messages{$id};
}

#|(
    Asks the server to subscribe to a particular message stream.
    Returns a promise that, when fulfilled, will return a Supply which will
    get the messages in that stream.
)
method subscribe ( Str:D $name!, $events, $compat = False --> Promise ) {
    my @promises = @$events.map: -> $event {
        self.send: {
            msg    => 'sub',
            name   => $name,
            params => [ $event, $compat ],
        }
    }

    Promise.allof(@promises).then: {
        $!supplier.Supply.grep: -> $body {
            ( $body<collection> // '' ) eq $name
                && $events.Set{ $body<fields><eventName> };
        }
    }
}

#|(
    Sends a call to a method to the server.
    Returns a promise that, when fulfilled, will return whatever the server
    responded to this method call.
)
method call ( Str:D $method!, $params --> Promise ) {
    self.send: { :$method, msg => 'method', params => [ $params, ] }
}

#|(
    Returns a live Supply that will receive every raw incoming message
    from the connection to the server.
)
method Supply ( --> Supply ) { $!supplier.Supply }

method !send ( %msg ) {
    die 'Not connected' unless self.connected;
    self!log: '>>', %msg;
    $!conn.send: %msg;
}

method !log ( Str $prefix, $payload ) {
    use JSON::Fast;
    return unless $!debug;
    note "$prefix { .&to-json: :!pretty, :sorted-keys }"
        with $payload.?Hash // $payload;
}
