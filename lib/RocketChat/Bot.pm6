use RocketChat::Realtime;
use RocketChat::Plugin;

unit class RocketChat::Bot;

my class RocketChat::Event {
    has $!data is built is required;
    has $!bot  is built is required;

    method type ( --> Str  ) { $!data<msg> // 'unknown' }

    method message ( --> Hash ) {
        return {} unless self.type eq 'changed';
        return $!data<fields><args>[0];
    }

    method meta ( --> Hash ) {
        return {} unless self.type eq 'changed';
        return $!data<fields><args>[1];
    }

    method reply ( $msg, :$in-thread, :$show-in-channel --> Promise ) {
        my $message = self.message
            or die "Cannot reply to an event of type { self.type }";

        my %args = :$msg, rid => $message<rid>;

        if $in-thread {
            %args<tmid> = $message<_id>;
            %args<tshow> = True if $show-in-channel;
        }

        $!bot.call: 'sendMessage', %args;
    }
}

has                      %!user;
has Supply               $!supply;
has Promise              $!done                    .= new;
has Int                  $!reconnect-limit is built = 0;
has Str                  $!name            is built;
has Str                  $!host            is built;
has Bool                 $!secure          is built;
has RocketChat::Plugin   @!plugins         is built;
has RocketChat::Realtime $!client handles 'call' .= new:
    server => ( $!secure ?? 'wss' !! 'ws' ) ~ "://$!host/websocket";

method TWEAK () { @!plugins .= map: { $_ // .new( bot => self ) } }

method run ( --> Promise ) {
    await self!connect;
    $!supply = await $!client.subscribe:
        'stream-room-messages', '__my_messages__';

    use Timer::Stopwatch;
    my $timer = Timer::Stopwatch.new;

    my $reconnects = 0;
    start react {
        whenever $!client.Supply.grep: { quietly .<msg> eq 'ping' } {
            # Pings come in every ~30 seconds
            # Wait up to 45 before timing out
            $timer.reset: 45;
        }
        whenever $timer {
            note 'Did not receive ping from server!';
            done if $!reconnect-limit && ++$reconnects > $!reconnect-limit;

            note 'Reconnecting...';
            $!client.reconnect: :!timeout;
        }
        whenever $!supply -> $payload {
            my $event = RocketChat::Event.new( data => $payload, bot => self );

            # When replying to a thread, the server sends the first message
            # in the thread as well. Since we don't know how to handle it
            # currently we just ignore it.
            next if $event.message<replies>.defined;

            for @!plugins {
                CATCH {
                    when X::Multi::NoMatch { } # Nothing to see here
                }

                last if .handle: $event
            }
        }
        whenever $!done { done }
    }
}

method stop ( |c --> Promise ) {
    $!client.close(|c).then: { $!done.keep };
}

method login ( *%creds --> Promise ) {
    await self!connect;
    $!client.call( 'login', %creds ).then: { %!user = .result }
}

method !connect ( --> Promise ) {
    return Promise.kept if $!client.connected;
    return $!client.connect;
}
