unit class RocketChat::REST;

use Cro::HTTP::Client;

has Str $.server is required;
has Int $.version = 1;
has Str $.username;
has Bool $.debug = False;

has Str $!token;
has Str $!user-id;

has $!client = Cro::HTTP::Client.new:
    content-type => 'application/json',
    http         => ['1.1'];

method TWEAK (:$token, :$user-id) {
    if $token && $user-id {
        $!token = $token;
        $!user-id = $user-id;
    }

    self!set-headers if self.logged-in;
}

method logged-in ( --> Bool ) { so $!user-id && $!token }

method login ( Str:D :$username, Str:D :$password, Int :$code? )  {
    return if self.logged-in;

    my %body;
    %body<user>     = $username;
    %body<password> = $password;
    %body<code>     = $code if $code;

    my $url = "$!server/api/v$!version/login";

    note "Querying $url..." if $!debug;

    my $resp = await $!client.post: $url, :%body;

    my $json = await $resp.body;

    $!token    = $json<data><authToken>;
    $!user-id  = $json<data><me><_id>;
    $!username = $json<data><me><username>;

    if $!debug {
        note "Auth token: $!token";
        note "User ID   : $!user-id";
    }

    self!set-headers;

    return $json;
}

method get ( Str:D $path ) {
    my $resp = await $!client.get: "$!server/api/v$!version$path";
    await $resp.body;
}

method !set-headers () {
    $!client.headers.push( 'X-Auth-Token' => $!token );
    $!client.headers.push( 'X-User-Id'    => $!user-id );
}
