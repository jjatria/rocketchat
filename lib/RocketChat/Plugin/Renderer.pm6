use RocketChat::Plugin;

unit class RocketChat::Plugin::Renderer does RocketChat::Plugin;

use Terminal::ANSIColor;

multi method handle ( $e where { .type eq 'changed' } --> Bool ) {
    my $msg = $e.message;
    my $ts = DateTime.new: $msg<ts><$date> / 1000, timezone => $*TZ;

    my $room-name = quietly $e.meta<roomType> eq 'd'
        ?? 'direct chat'
        !! '#' ~ ( $e.meta<roomName> // '...' );

    say "\n", BOLD,
        sprintf(
            '%s (@%s) on %s - %s',
            $msg<u><name> // $msg<u><username>,
            $msg<u><username>,
            $room-name,
            $ts.hh-mm-ss,
        ),
        RESET;

    say $msg<msg>;

    return False;
}
