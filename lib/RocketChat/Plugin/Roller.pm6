use RocketChat::Plugin;

unit class RocketChat::Plugin::Roller does RocketChat::Plugin;

my class Dice::Roll {
    has Int  $!dice      is built = 1;
    has Int  $!keep      is built = 0;
    has Int  $!sides     is built;
    has Bool $!exploding is built;
    has Int  @!rolls;

    submethod TWEAK () {
        die 'Cannot keep more dice than were rolled' if $!keep > $!dice;
    }

    method roll () {
        @!rolls = gather for 1 .. $!dice {
            take my $roll = ( 1 .. $!sides ).pick;
            redo if $!exploding && $roll == $!sides;
        }
    }

    method Int ( --> Int ) {
        self.roll unless @!rolls;
        return [+] self!kept;
    }

    method Str ( --> Str ) {
        self.roll unless @!rolls;

        return ~@!rolls.head if @!rolls == 1;

        my $keep = self!kept.BagHash;

        return "( { .join(' + ') } )"
            given @!rolls.map: { $keep{$_}-- ?? $_ !! "~~$_~~" }
    }

    method !kept () {
        $!keep ?? @!rolls.sort.tail($!keep) !! @!rolls;
    }
}

my grammar Roll {
    rule TOP { <operation> { make $<operation>.made } }

    token operation {
        <operand>+ % <op>
        {
            my @ops = $<op>».made;

            my $num = $<operand>.map( *.made.Int )
                .reduce({ @ops.shift.( $^a, $^b ) }).Num;

            my $str = roundrobin(
                $<operand>.map( *.made.Str ),
                $<op>».Str,
            ).join(' ') ~ " = $num";

            make NumStr.new( $num, $str );
        }
    }

    token operand {
        | <roll>   { make $<roll>.made   }
        | <number> { make $<number>.made }
    }

    token roll {
        <number> 'd' <number> <modifier>?
        {
            my %args = (
                sides     => +$<number>[1],
                dice      => +$<number>[0],
                exploding => ( $<modifier> // '' ) eq '!',
            );

            given $<modifier> {
                %args<keep> = +.<keep>               when .<keep>.so;
                %args<keep> = %args<dice> - +.<drop> when .<drop>.so;
            }

            make Dice::Roll.new: |%args;
        }
    }

    token number { \d+ { make ~$/ } }

    token modifier {
        | '!'
        | 'k' $<keep> = \d+
        | 'd' $<drop> = \d+
    }

    proto rule op {*}
          rule op:sym<+> { '+' { make * + * } }
          rule op:sym<-> { '-' { make * - * } }
}

multi method command ( $ where * eq 'r' | 'roll', $e --> Bool ) {
    with $e.message<msg>.&parse {
        $e.reply: .Str, :in-thread, :show-in-channel;
        return True;
    }

    return False;
}

multi method command ( $ where * eq 'rr' | 'multiroll', $e --> Bool ) {
    return False unless $e.message<msg> ~~ /
        [ 'rr' | 'multiroll' ] <.ws>
        $<iter> = (\d+) <.ws>
        $<msg>  = (.*)
    /;

    my $msg = '';
    for ^$<iter> {
        $msg ~= "* $_\n" with $<msg>.&parse;
    }

    $e.reply: $msg, :in-thread, :show-in-channel;

    return True;
}

sub parse ($msg) { Roll.parse( $msg.subst(/ ^ '!' \w+ \s+ /, '' ) ).made }
