use RocketChat::Plugin;

unit class RocketChat::Plugin::ROT does RocketChat::Plugin;

multi method command ( $ where / ^ 'rot' \d ** ^3 $ /, $e --> Bool ) {
    return False unless $e.message<msg> ~~ /
        'rot'
        $<shift> = (\d*) \s+
        $<msg>   = (.*)
    /;

    with try rot( ~$<msg>, +$<shift> || 13 ) -> $reply {
        $e.reply: $reply, :in-thread, :show-in-channel;
        return True;
    }

    return False;
}

my sub rot ( Str $str!, Int $n = 13 --> Str ) {
    my $m = 26 - $n;
    $str.comb.map( -> $s {
        my $o = ord($s);

        my $c =  0 <= ( $o - 97 )  < $m ||   0 <= ( $o - 65 ) < $m ??  $n
            !! -$n <= ( $o - 122 ) <= 0 || -$n <= ( $o - 90 ) <= 0 ?? -$m
            !! 0;

        chr( $o + $c );
    }).join;
}
