unit role RocketChat::Plugin is export;

has $.bot;

multi method handle ( $e ) {
    if quietly $e.message<msg> ~~ / ^ '!' $<command> = [ \w+ ] / {
        CATCH {
            when X::Multi::NoMatch { } # Nothing to see here
        }

        .return with self.?command( ~$<command>, $e );
    }

    nextsame;
}
